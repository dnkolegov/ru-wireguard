// GoGOST -- Pure Go GOST cryptographic functions library
// Copyright (C) 2015-2020 Sergey Matveev <stargrave@stargrave.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Multilinear Galois Mode (MGM) block cipher mode.
package mgm

import (
	"crypto/cipher"
	"crypto/hmac"
	"encoding/binary"
	"errors"
	"math/big"
)

var (
	R64  *big.Int = big.NewInt(0)
	R128 *big.Int = big.NewInt(0)
)

func init() {
	R64.SetBytes([]byte{
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1b,
	})
	R128.SetBytes([]byte{
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x87,
	})
}

type MGM struct {
	maxSize   uint64
	cipher    cipher.Block
	blockSize int
	tagSize   int
	maxBit int
	r      *big.Int
}

func NewMGM(cipher cipher.Block, tagSize int) (cipher.AEAD, error) {
	blockSize := cipher.BlockSize()
	if !(blockSize == 8 || blockSize == 16) {
		return nil, errors.New("gogost/mgm: only 64/128 blocksizes allowed")
	}
	if tagSize < 4 || tagSize > blockSize {
		return nil, errors.New("gogost/mgm: invalid tag size")
	}
	mgm := MGM{
		maxSize:   uint64(1<<uint(blockSize*8/2) - 1),
		cipher:    cipher,
		blockSize: blockSize,
		tagSize:   tagSize,
	}
	if blockSize == 8 {
		mgm.maxBit = 64 - 1
		mgm.r = R64
	} else {
		mgm.maxBit = 128 - 1
		mgm.r = R128
	}
	return &mgm, nil
}

func (mgm *MGM) NonceSize() int {
	return mgm.blockSize
}

func (mgm *MGM) Overhead() int {
	return mgm.tagSize
}

func incr(data []byte) {
	for i := len(data) - 1; i >= 0; i-- {
		data[i]++
		if data[i] != 0 {
			return
		}
	}
}

func xor(dst, src1, src2 []byte) {
	for i := 0; i < len(src1); i++ {
		dst[i] = src1[i] ^ src2[i]
	}
}

func (mgm *MGM) validateNonce(nonce []byte) {
	if len(nonce) != mgm.blockSize {
		panic("nonce length must be equal to cipher's blocksize")
	}
	if nonce[0]&0x80 > 0 {
		panic("nonce must not have higher bit set")
	}
}

func (mgm *MGM) validateSizes(text, additionalData []byte) {
	if len(text) == 0 && len(additionalData) == 0 {
		panic("at least either *text or additionalData must be provided")
	}
	if uint64(len(additionalData)) > mgm.maxSize {
		panic("additionalData is too big")
	}
	if uint64(len(text)+len(additionalData)) > mgm.maxSize {
		panic("*text with additionalData are too big")
	}
}

func (mgm *MGM) auth(out, text, ad, icn []byte) {
	sum := make([]byte, mgm.blockSize)
	bufP := make([]byte, mgm.blockSize)
	bufC := make([]byte, mgm.blockSize)
	padded := make([]byte, mgm.blockSize)

	for i := 0; i < mgm.blockSize; i++ {
		sum[i] = 0
	}
	adLen := len(ad) * 8
	textLen := len(text) * 8
	icn[0] |= 0x80
	mgm.cipher.Encrypt(bufP, icn) // Z_1 = E_K(1 || ICN)
	for len(ad) >= mgm.blockSize {
		mgm.cipher.Encrypt(bufC, bufP) // H_i = E_K(Z_i)
		xor(                                   // sum (xor)= H_i (x) A_i
			sum,
			sum,
			mgm.mul(bufC, ad[:mgm.blockSize]),
		)
		incr(bufP[:mgm.blockSize/2]) // Z_{i+1} = incr_l(Z_i)
		ad = ad[mgm.blockSize:]
	}
	if len(ad) > 0 {
		copy(padded, ad)
		for i := len(ad); i < mgm.blockSize; i++ {
			padded[i] = 0
		}
		mgm.cipher.Encrypt(bufC, bufP)
		xor(sum, sum, mgm.mul(bufC, padded))
		incr(bufP[:mgm.blockSize/2])
	}

	for len(text) >= mgm.blockSize {
		mgm.cipher.Encrypt(bufC, bufP) // H_{h+j} = E_K(Z_{h+j})
		xor(                                   // sum (xor)= H_{h+j} (x) C_j
			sum,
			sum,
			mgm.mul(bufC, text[:mgm.blockSize]),
		)
		incr(bufP[:mgm.blockSize/2]) // Z_{h+j+1} = incr_l(Z_{h+j})
		text = text[mgm.blockSize:]
	}
	if len(text) > 0 {
		copy(padded, text)
		for i := len(text); i < mgm.blockSize; i++ {
			padded[i] = 0
		}
		mgm.cipher.Encrypt(bufC, bufP)
		xor(sum, sum, mgm.mul(bufC, padded))
		incr(bufP[:mgm.blockSize/2])
	}

	mgm.cipher.Encrypt(bufP, bufP) // H_{h+q+1} = E_K(Z_{h+q+1})
	// len(A) || len(C)
	if mgm.blockSize == 8 {
		binary.BigEndian.PutUint32(bufC, uint32(adLen))
		binary.BigEndian.PutUint32(bufC[mgm.blockSize/2:], uint32(textLen))
	} else {
		binary.BigEndian.PutUint64(bufC, uint64(adLen))
		binary.BigEndian.PutUint64(bufC[mgm.blockSize/2:], uint64(textLen))
	}
	// sum (xor)= H_{h+q+1} (x) (len(A) || len(C))
	xor(sum, sum, mgm.mul(bufP, bufC))
	mgm.cipher.Encrypt(bufP, sum) // E_K(sum)
	copy(out, bufP[:mgm.tagSize]) // MSB_S(E_K(sum))
}

func (mgm *MGM) crypt(out, in, icn []byte) {
	icn[0] &= 0x7F
	bufP := make([]byte, mgm.blockSize)
	bufC := make([]byte, mgm.blockSize)
	mgm.cipher.Encrypt(bufP, icn) // Y_1 = E_K(0 || ICN)
	for len(in) >= mgm.blockSize {
		mgm.cipher.Encrypt(bufC, bufP) // E_K(Y_i)
		xor(out, bufC, in)             // C_i = P_i (xor) E_K(Y_i)
		incr(bufP[mgm.blockSize/2:])   // Y_i = incr_r(Y_{i-1})
		out = out[mgm.blockSize:]
		in = in[mgm.blockSize:]
	}
	if len(in) > 0 {
		mgm.cipher.Encrypt(bufC, bufP)
		xor(out, in, bufC)
	}
}

func (mgm *MGM) Seal(dst, nonce, plaintext, additionalData []byte) []byte {
	mgm.validateNonce(nonce)
	mgm.validateSizes(plaintext, additionalData)
	if uint64(len(plaintext)) > mgm.maxSize {
		panic("plaintext is too big")
	}
	ret, out := sliceForAppend(dst, len(plaintext)+mgm.tagSize)

	icn := make([]byte, mgm.blockSize)
	copy(icn, nonce)

	mgm.crypt(out, plaintext, icn)

	mgm.auth(
		out[len(plaintext):len(plaintext)+mgm.tagSize],
		out[:len(plaintext)],
		additionalData,
		icn,
	)
	return ret
}

func (mgm *MGM) Open(dst, nonce, ciphertext, additionalData []byte) ([]byte, error) {
	mgm.validateNonce(nonce)
	mgm.validateSizes(ciphertext, additionalData)
	if uint64(len(ciphertext)-mgm.tagSize) > mgm.maxSize {
		panic("ciphertext is too big")
	}
	ret, out := sliceForAppend(dst, len(ciphertext)-mgm.tagSize)
	ct := ciphertext[:len(ciphertext)-mgm.tagSize]
	icn := make([]byte, mgm.blockSize)
	sum := make([]byte, mgm.blockSize)
	copy(icn, nonce)
	mgm.auth(sum, ct, additionalData, icn)
	if !hmac.Equal(sum[:mgm.tagSize], ciphertext[len(ciphertext)-mgm.tagSize:]) {
		return nil, errors.New("gogost/mgm: invalid authentication tag")
	}
	mgm.crypt(out, ct, icn)
	return ret, nil
}

/* SPDX-License-Identifier: MIT
 *
 * Copyright (C) 2017-2020 WireGuard LLC. All Rights Reserved.
 */

package device

import (
	"bytes"
	"encoding/binary"
	"testing"
)

func TestCurveWrappers(t *testing.T) {
	sk1, err := newPrivateKey()
	assertNil(t, err)

	sk2, err := newPrivateKey()
	assertNil(t, err)

	pk1 := sk1.publicKey()
	pk2 := sk2.publicKey()

	ss1 := sk1.sharedSecret(pk2)
	ss2 := sk2.sharedSecret(pk1)

	if ss1 != ss2 {
		t.Fatal("Failed to compute shared secret")
	}
}

func TestNoiseHandshakeFixed(t *testing.T) {
	dev1 := randDevice(t)
	dev2 := randDevice(t)

	//TODO(imanashev): rewrite with mock
	newPrivateKeyBackup := newPrivateKey
	i := 0
	newPrivateKey = func() (sk NoisePrivateKey, err error) {
		values := []string{
			"2122232425262728292a2b2c2d2e2f303132333435363738393a3b3c3d3e3f40", // ek1
			"202122232425262728292a2b2c2d2e2f303132333435363738393a3b3c3d3e3f", // ek2
		}
		var key NoisePrivateKey
		key.FromHex(values[i])
		i++
		copy(sk[:], key[:])
		return sk, nil
	}

	var sk1 NoisePrivateKey
	var sk2 NoisePrivateKey
	sk1.FromHex("000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f")
	sk2.FromHex("0102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f20")
	dev1.SetPrivateKey(sk1)
	dev2.SetPrivateKey(sk2)

	defer dev1.Close()
	defer dev2.Close()

	peer1, _ := dev2.NewPeer(dev1.staticIdentity.privateKey.publicKey())
	peer2, _ := dev1.NewPeer(dev2.staticIdentity.privateKey.publicKey())

	assertEqual(
		t,
		peer1.handshake.precomputedStaticStatic[:],
		peer2.handshake.precomputedStaticStatic[:],
	)

	/* simulate handshake */

	// initiation message

	t.Log("exchange initiation message")

	msg1, err := dev1.CreateMessageInitiation(peer2)
	assertNil(t, err)

	packet := make([]byte, 0, 256)
	writer := bytes.NewBuffer(packet)
	err = binary.Write(writer, binary.LittleEndian, msg1)
	assertNil(t, err)
	peer := dev2.ConsumeMessageInitiation(msg1)
	if peer == nil {
		t.Fatal("handshake failed at initiation message")
	}

	assertEqual(
		t,
		peer1.handshake.chainKey[:],
		peer2.handshake.chainKey[:],
	)
	chainKeyAfterFirstMessage := []byte{
		0x69, 0xb5, 0xcc, 0x4a, 0x8d, 0x69, 0x56, 0x32, 0x5b, 0x0a, 0xb2, 0x93, 0x9a, 0x3f, 0xad, 0x63,
		0x40, 0x00, 0xcd, 0xb9, 0xe0, 0xb6, 0x47, 0x10, 0x1a, 0x32, 0x58, 0x05, 0x77, 0x2f, 0xe1, 0x3d,
	}
	assertEqual(
		t,
		peer1.handshake.chainKey[:],
		chainKeyAfterFirstMessage,
	)

	assertEqual(
		t,
		peer1.handshake.hash[:],
		peer2.handshake.hash[:],
	)

	// response message

	t.Log("exchange response message")

	msg2, err := dev2.CreateMessageResponse(peer1)
	assertNil(t, err)

	peer = dev1.ConsumeMessageResponse(msg2)
	if peer == nil {
		t.Fatal("handshake failed at response message")
	}

	assertEqual(
		t,
		peer1.handshake.chainKey[:],
		peer2.handshake.chainKey[:],
	)
	chainKeyAfterSecondMessage := []byte{
		0x22, 0xbf, 0x0e, 0x8b, 0xe8, 0x21, 0x32, 0xd8, 0xee, 0x3f, 0x2d, 0x0e, 0xc5, 0x2d, 0x27, 0xf5,
		0x42, 0xb5, 0x68, 0x9b, 0x77, 0x8e, 0xb8, 0xf2, 0xb1, 0x96, 0x65, 0xb6, 0x94, 0x94, 0xe2, 0x7c,
	}
	assertEqual(
		t,
		peer1.handshake.chainKey[:],
		chainKeyAfterSecondMessage,
	)

	assertEqual(
		t,
		peer1.handshake.hash[:],
		peer2.handshake.hash[:],
	)

	// key pairs

	t.Log("deriving keys")

	err = peer1.BeginSymmetricSession()
	if err != nil {
		t.Fatal("failed to derive keypair for peer 1", err)
	}

	err = peer2.BeginSymmetricSession()
	if err != nil {
		t.Fatal("failed to derive keypair for peer 2", err)
	}

	key1 := peer1.keypairs.loadNext()
	key2 := peer2.keypairs.current

	// encrypting / decryption test

	t.Log("test key pairs")

	func() {
		testMsg := []byte{
			0x72, 0x75, 0x20, 0x77, 0x69, 0x72, 0x65, 0x67, 0x75, 0x61, 0x72, 0x64, 0x20, 0x74, 0x65, 0x73,
			0x74, 0x20, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x20, 0x31, 0x2d, 0x2d, 0x2d, 0x2d, 0x2d,
		}
		encryptedTestMsg := []byte{
			0x9d, 0x65, 0x2a, 0xa2, 0x8d, 0x7c, 0x6a, 0xbe, 0xf1, 0xf7, 0xf4, 0xdb, 0xdc, 0x8b, 0x37, 0x45,
			0xaf, 0x77, 0x6c, 0xc2, 0xef, 0x10, 0x19, 0x90, 0x66, 0xbc, 0xaf, 0x17, 0x92, 0x03, 0xed, 0xd5,
			0x28, 0x26, 0x56, 0xb1, 0x4e, 0xf8, 0x99, 0x45, 0x94, 0x2b, 0xdf, 0x8a, 0x10, 0x63, 0xf8, 0x68,
		}
		var err error
		var out []byte
		var nonce [16]byte
		additionalData := make([]byte, 12)
		binary.LittleEndian.PutUint32(additionalData[0:], MessageTransportType)
		binary.LittleEndian.PutUint32(additionalData[4:], 1)
		binary.LittleEndian.PutUint32(additionalData[8:], 2)

		out = key1.send.Seal(out, nonce[:], testMsg, additionalData)
		assertEqual(t, out, encryptedTestMsg)
		out, err = key2.receive.Open(out[:0], nonce[:], out, additionalData)
		assertNil(t, err)
		assertEqual(t, out, testMsg)
	}()

	func() {
		testMsg := []byte{
			0x72, 0x75, 0x20, 0x77, 0x69, 0x72, 0x65, 0x67, 0x75, 0x61, 0x72, 0x64, 0x20, 0x74, 0x65, 0x73,
			0x74, 0x20, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x20, 0x32, 0x2d, 0x2d, 0x2d, 0x2d, 0x2d,
		}
		encryptedTestMsg := []byte{
			0xc1, 0x1b, 0x69, 0xa0, 0x43, 0xff, 0x5b, 0x77, 0xaf, 0xd1, 0xb6, 0xb1, 0xb0, 0xec, 0xaa, 0x3a,
			0x3f, 0x6e, 0x46, 0xcc, 0x40, 0x81, 0x4b, 0x50, 0xd9, 0x9b, 0x49, 0x17, 0x9e, 0x00, 0xff, 0xa7,
			0x3c, 0x14, 0x18, 0x6c, 0x57, 0x49, 0x54, 0xfa, 0x26, 0x52, 0x2f, 0x50, 0xee, 0x3b, 0x2f, 0x8b,
		}
		var err error
		var out []byte
		var nonce [16]byte
		additionalData := make([]byte, 12)
		binary.LittleEndian.PutUint32(additionalData[0:], MessageTransportType)
		binary.LittleEndian.PutUint32(additionalData[4:], 2)
		binary.LittleEndian.PutUint32(additionalData[8:], 1)

		out = key2.send.Seal(out, nonce[:], testMsg, additionalData)
		assertEqual(t, out, encryptedTestMsg)
		out, err = key1.receive.Open(out[:0], nonce[:], out, additionalData)
		assertNil(t, err)
		assertEqual(t, out, testMsg)
	}()

	newPrivateKey = newPrivateKeyBackup
}

func TestNoiseHandshakeRandom(t *testing.T) {
	dev1 := randDevice(t)
	dev2 := randDevice(t)

	defer dev1.Close()
	defer dev2.Close()

	peer1, _ := dev2.NewPeer(dev1.staticIdentity.privateKey.publicKey())
	peer2, _ := dev1.NewPeer(dev2.staticIdentity.privateKey.publicKey())

	assertEqual(
		t,
		peer1.handshake.precomputedStaticStatic[:],
		peer2.handshake.precomputedStaticStatic[:],
	)

	/* simulate handshake */

	// initiation message

	t.Log("exchange initiation message")

	msg1, err := dev1.CreateMessageInitiation(peer2)
	assertNil(t, err)

	packet := make([]byte, 0, 256)
	writer := bytes.NewBuffer(packet)
	err = binary.Write(writer, binary.LittleEndian, msg1)
	assertNil(t, err)
	peer := dev2.ConsumeMessageInitiation(msg1)
	if peer == nil {
		t.Fatal("handshake failed at initiation message")
	}

	assertEqual(
		t,
		peer1.handshake.chainKey[:],
		peer2.handshake.chainKey[:],
	)

	assertEqual(
		t,
		peer1.handshake.hash[:],
		peer2.handshake.hash[:],
	)

	// response message

	t.Log("exchange response message")

	msg2, err := dev2.CreateMessageResponse(peer1)
	assertNil(t, err)

	peer = dev1.ConsumeMessageResponse(msg2)
	if peer == nil {
		t.Fatal("handshake failed at response message")
	}

	assertEqual(
		t,
		peer1.handshake.chainKey[:],
		peer2.handshake.chainKey[:],
	)

	assertEqual(
		t,
		peer1.handshake.hash[:],
		peer2.handshake.hash[:],
	)

	// key pairs

	t.Log("deriving keys")

	err = peer1.BeginSymmetricSession()
	if err != nil {
		t.Fatal("failed to derive keypair for peer 1", err)
	}

	err = peer2.BeginSymmetricSession()
	if err != nil {
		t.Fatal("failed to derive keypair for peer 2", err)
	}

	key1 := peer1.keypairs.loadNext()
	key2 := peer2.keypairs.current

	// encrypting / decryption test

	t.Log("test key pairs")

	func() {
		testMsg := []byte("wireguard test message 1")
		var err error
		var out []byte
		var nonce [16]byte
		out = key1.send.Seal(out, nonce[:], testMsg, nil)
		out, err = key2.receive.Open(out[:0], nonce[:], out, nil)
		assertNil(t, err)
		assertEqual(t, out, testMsg)
	}()

	func() {
		testMsg := []byte("wireguard test message 2")
		var err error
		var out []byte
		var nonce [16]byte
		out = key2.send.Seal(out, nonce[:], testMsg, nil)
		out, err = key1.receive.Open(out[:0], nonce[:], out, nil)
		assertNil(t, err)
		assertEqual(t, out, testMsg)
	}()
}

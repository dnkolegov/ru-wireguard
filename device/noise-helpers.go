/* SPDX-License-Identifier: MIT
 *
 * Copyright (C) 2017-2020 WireGuard LLC. All Rights Reserved.
 */

package device

import (
	"crypto/hmac"
	"crypto/rand"
	"crypto/subtle"
	"math/big"

	"gitlab.com/dnkolegov/ru-wireguard/crypto/gogost/gost3410"
	"gitlab.com/dnkolegov/ru-wireguard/crypto/gogost/gost34112012256"
)

const (
	KDFLabel1 = "KDF_GOST_R_3411_2012_256_LABEL_1"
	KDFLabel2 = "KDF_GOST_R_3411_2012_256_LABEL_2"
	KDFLabel3 = "KDF_GOST_R_3411_2012_256_LABEL_3"
)

func HMAC(sum *[gost34112012256.Size]byte, key [gost34112012256.Size]byte, i, label, seed, l []byte) {
	mac := hmac.New(gost34112012256.New, key[:])
	mac.Write(i)
	mac.Write(label)
	mac.Write([]byte{0x00})
	mac.Write(seed)
	mac.Write(l)
	mac.Write([]byte{0x00})
	mac.Sum(sum[:0])
}

func KDF1(t0 *[gost34112012256.Size]byte, key [gost34112012256.Size]byte, input []byte) {
	HMAC(t0, key, []byte{0x01}, []byte(KDFLabel1), input, []byte{0x01})
}

func KDF2(t0, t1 *[gost34112012256.Size]byte, key [gost34112012256.Size]byte, input []byte) {
	HMAC(t0, key, []byte{0x01}, []byte(KDFLabel2), input, []byte{0x02})
	HMAC(t1, key, []byte{0x02}, []byte(KDFLabel2), input, []byte{0x02})
}

func KDF3(t0, t1, t2 *[gost34112012256.Size]byte, key [gost34112012256.Size]byte, input []byte) {
	HMAC(t0, key, []byte{0x01}, []byte(KDFLabel3), input, []byte{0x03})
	HMAC(t1, key, []byte{0x02}, []byte(KDFLabel3), input, []byte{0x03})
	HMAC(t2, key, []byte{0x03}, []byte(KDFLabel3), input, []byte{0x03})
}

func isZero(val []byte) bool {
	acc := 1
	for _, b := range val {
		acc &= subtle.ConstantTimeByteEq(b, 0)
	}
	return acc == 1
}

/* This function is not used as pervasively as it should because this is mostly impossible in Go at the moment */
func setZero(arr []byte) {
	for i := range arr {
		arr[i] = 0
	}
}

var (
	Curve *gost3410.Curve = gost3410.CurveIdtc26gost34102012256paramSetA()
	Zero  *big.Int        = big.NewInt(0)
	UKM   *big.Int        = big.NewInt(1)
)

func IsOnCurve(x, y *big.Int) bool {
	// y² = x³ + ax + b
	r1 := new(big.Int).Mul(y, y)
	r1.Mod(r1, Curve.P)

	r2 := new(big.Int).Mul(x, x)
	r2.Add(r2, Curve.A)
	r2.Mul(r2, x)
	r2.Add(r2, Curve.B)
	r2.Mod(r2, Curve.P)
	if r2.Cmp(Zero) < 0 {
		r2.Add(r2, Curve.P)
	}
	return r1.Cmp(r2) == 0
}

func MarshalCompressed(x, y *big.Int) []byte {
	compressed := make([]byte, Curve.PointSize()+1)
	compressed[0] = byte(y.Bit(0)) | 2
	x.FillBytes(compressed[1:])
	return compressed
}

func UnmarshalCompressed(data []byte) (x, y *big.Int) {
	if len(data) != Curve.PointSize()+1 {
		return nil, nil
	}
	if data[0] != 2 && data[0] != 3 {
		return nil, nil
	}
	x = new(big.Int).SetBytes(data[1:])
	if x.Cmp(Curve.P) >= 0 {
		return nil, nil
	}
	y = new(big.Int).Mul(x, x)
	y.Add(y, Curve.A)
	y.Mul(y, x)
	y.Add(y, Curve.B)
	y.Mod(y, Curve.P)
	y = y.ModSqrt(y, Curve.P)
	if y == nil {
		return nil, nil
	}
	if byte(y.Bit(0)) != data[0]&1 {
		y.Neg(y).Mod(y, Curve.P)
	}
	if !IsOnCurve(x, y) {
		return nil, nil
	}
	return
}

var newPrivateKey = func() (sk NoisePrivateKey, err error) {
	newKey, err := gost3410.GenPrivateKey(Curve, rand.Reader)
	if err != nil {
		return
	}
	copy(sk[:], newKey.Raw())
	return
}

func (sk *NoisePrivateKey) publicKey() (pk NoisePublicKey) {
	ask, _ := gost3410.NewPrivateKey(Curve, (*sk)[:])
	apk, _ := ask.PublicKey()
	copy(pk[:], MarshalCompressed(apk.X, apk.Y))
	return
}

func (sk *NoisePrivateKey) sharedSecret(pk NoisePublicKey) (ss [NoisePrivateKeySize]byte) {
	ask, _ := gost3410.NewPrivateKey(Curve, (*sk)[:])
	apk := new(gost3410.PublicKey)

	apk.C = Curve
	apk.X, apk.Y = UnmarshalCompressed(pk[:])
	if apk.X == nil && apk.Y == nil {
		return
	}

	kek, _ := ask.KEK2012256(apk, UKM)
	copy(ss[:], kek)
	return
}

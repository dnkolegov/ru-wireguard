// +build !linux android

package device

import (
	"gitlab.com/dnkolegov/ru-wireguard/conn"
	"gitlab.com/dnkolegov/ru-wireguard/rwcancel"
)

func (device *Device) startRouteListener(bind conn.Bind) (*rwcancel.RWCancel, error) {
	return nil, nil
}

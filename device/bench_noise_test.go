package device

import (
	"testing"
)

func randDeviceUnsafe() *Device {
	sk, _ := newPrivateKey()
	tun := newDummyTUN("dummy")
	logger := NewLogger(LogLevelError, "")
	device := NewDevice(tun, logger)
	device.SetPrivateKey(sk)
	return device
}

func BenchmarkNoiseHandshake(b *testing.B) {
	b.StopTimer()
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		dev1 := randDeviceUnsafe()
		dev2 := randDeviceUnsafe()

		peer1, _ := dev2.NewPeer(dev1.staticIdentity.privateKey.publicKey())
		peer2, _ := dev1.NewPeer(dev2.staticIdentity.privateKey.publicKey())

		/* simulate handshake */
		b.StartTimer()

		// initiation message
		msg1, _ := dev1.CreateMessageInitiation(peer2)
		dev2.ConsumeMessageInitiation(msg1)

		// response message
		msg2, _ := dev2.CreateMessageResponse(peer1)
		dev1.ConsumeMessageResponse(msg2)

		// key pairs
		peer1.BeginSymmetricSession()
		peer2.BeginSymmetricSession()

		key1 := peer1.keypairs.loadNext()
		key2 := peer2.keypairs.current

		// encrypting / decryption test
		testMsg1 := []byte("wireguard test message 1")
		var out1 []byte
		var nonce1 [16]byte
		out1 = key1.send.Seal(out1, nonce1[:], testMsg1, nil)
		out1, _ = key2.receive.Open(out1[:0], nonce1[:], out1, nil)

		testMsg2 := []byte("wireguard test message 2")
		var out2 []byte
		var nonce2 [16]byte
		out2 = key2.send.Seal(out2, nonce2[:], testMsg2, nil)
		out2, _ = key1.receive.Open(out2[:0], nonce2[:], out2, nil)

		b.StopTimer()
		dev1.Close()
		dev2.Close()
	}
}
